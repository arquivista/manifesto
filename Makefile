mat:
	@find -type f -name *.png -exec mat2 {} \;
	@find -type f -name *.jpg -exec mat2 {} \;

serve:
	@hugo server -Dwv -d public

compile: mat
	@hugo

deploy:
	@rsync --delete -avz public/ arquivista:/var/sites/arquivista/

publish: compile deploy
