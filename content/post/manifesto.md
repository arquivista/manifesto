+++
date = "2019-01-08T01:00:00-07:00"
title = "Manifesto Arquivista"
+++

{{< figure src="/images/museu-nacional.jpg" alt="Museu Nacional em chamas - 2018" >}}

> "Quem controla o passado, controla o futuro; quem controla o presente controla o passado"

> -- George Orwell, 1984

* * *

Quem controla o presente agora?

O que podemos fazer juntos para arquivar nosso presente e impedir que
reescrevam nosso passado e definam o nosso futuro?

A Internet é o mais novo campo de disputa político, passando da condição de
espaço marginal em que podíamos discutir e sonharmos juntos por novos futuros
políticos para um espaço central da política institucional que se utiliza das
redes para fabricar e disseminar conteúdos sem qualquer fundamento factual e
histórico. A política no espaço público online tornou-se um sala de espelhos
invertidos e distorcidos, extremamente eficazes para eleger governos.

Não resta tempo a perder! Além do trabalho cotidiano de compartilhar e informar
a todas e a todos no cotidiano: podemos também documentar como um recurso de
salvaguarda. Ao coletarmos dados existentes, podemos melhor sustentar pesquisas
e colaborar com jornalistas com informação confiável. Cada um de nós pode, a
sua maneira e de acordo com suas possibilidades, contribuir para criar essa
base de dados arquivística.

O trabalho de Arquivista é simples e direto: consiste em juntar dados
existentes sobre o governo bases de dados comunais, compartilhadas e anônimas
para que todos e todas possam se beneficiar na verificaçãoo de fatos.

## Quem pode participar?

Qualquer pessoa preocupada com a existência da informação produzida no Brasil.

## Como participar?

Nós criamos [um pequeno roteiro](/acumule) para começarmos a coletar dados
públicos sobre o governo para criação de backups.  Como não sabemos o futuro
desses dados, a melhor coisa é começarmos a coleta de forma distribuída e
anônima.

## Por que participar?

Em seu sentido clássico, a política é a arte de bem viver em grupo. A prática
arquivística tem um papel importante no processo de criação deste mundo em que
todos queremos viver: documentar o Brasil atual é impedir que versões sejam
fabricadas, distribuídas, e reinventadas para benefício de minusculas parcelas
da população e em prol da manutenção de certos governos. 

Arquivar é contribuir para a criação de uma nova política em que a Internet
servirá de ferramenta de informação e sociabilidade, não de desinformação,
vigilância e propagação de ódio.

O presente e o nosso futuro depende da nossa memória coletiva.

Este convite é para você, **Arquivista**, para contribuir com arquivos virtuais e
virtuosos para registrar e recontar as histórias que não deixaremos que sejam
esquecidas!
