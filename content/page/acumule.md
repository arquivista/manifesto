+++
date = "2019-01-04T01:00:00-07:00"
title = "Acumule dados!"
url = "acumule"
+++

## Como salvar conteúdo da internet

É muito fácil colaborar com a memória digital! Vamos das formas mais básicas para as mais avançadas:

1. Tirando um "print" (captura de tela), seja no celular ou no computador. Caso queira privacidade nesta tarefa, utilize o [Navegador Tor](https://www.torproject.org).
2. [Salvando uma páginas web usando seu navegador](https://support.mozilla.org/pt-BR/kb/como-salvar-uma-pagina-web)
3. Salvando um site inteiro usando ferramentas como o [HTTRack](http://www.httrack.com/) ou [HTTraQT](http://httraqt.sourceforge.net/)([pacote para o Debian](https://packages.debian.org/stable/httraqt)).
4. Usar ferramentas mais complexas como o [wpull](https://github.com/ArchiveTeam/wpull/) e o [grab-site](https://github.com/ludios/grab-site/).

Por quê você não começa a salvar uma cópia de tudo o que você lê na internet e
que considere importante?

## Como preservar o conteúdo digital

Baixar o conteúdo é só a primeira parte do esforço. A preservação é fundamental
para que os conteúdos estejam disponíveis no futuro.

Você pode colaborar de acordo com a sua capacidade e disponibilidade de tempo.
Aqui damos dicas, das mais fáceis para as mais complexas:

1. Faça backups do que você salvou em discos e pendrives. Quando mais cópias, melhor. Troque esses discos com seus amigos periodicamente, para ter redundância dos dados.
2. Grave tudo em pastas organizadas por site e por data.
3. Etiquete os discos e os pendrives, para que você consiga distingui-los facilmente.
4. Faça um catálogo do conteúdo disponível, que pode ser de um simples arquivo de índice até planilhas ou banco de dados. Grave uma cpópia do catálogo nos seus discos e pendrives.
5. Teste seus discos e pendrives periodicamente, para evitar que dados sejam perdidos por falhas nos equipamentos, aumentando assim a resiliência do seu acervo.
6. Envie cópias do seu acervo para repositórios online como o [Web Archive](https://archive.org) ou mesmo seu disco virtual.

## Como guardar conteúdo impresso

Você também pode guardar conteúdo à moda antiga:

1. Armazene livros, revistas, artigos, panfletos...
2. Use técnicas existentes de biblioteconomia para organizar e catalogar seu acervo.
3. Use métodos da preservação de arquivos para garantir a durabilidade do seu acervo.

## O que salvar?

Salve-se quem puder! Salvemos o que pudermos! Salvemos como pudermos!

1. Sites do governo, que são modificados constantemente. Tendo cópias periódicas e distribuídas, podemos comparar versões e identificar mudanças, seja conteúdo adicionado ou removido.
2. Sites, blogs e publicações que possam ser ameaçados de censura.
3. Postagens de redes sociais de personalidades e instituições que ameaçem a democracia e que podem apagar seus próprios conteúdos como parte de campanhas de diversionismo e desinformação.
4. Quaisquer outras informações de interesse público que possam desaparece da internet, como software livre/aberto e conteúdos sob domínio público.

A internet é um sistema onde a informação tem volatilidade: nada garante que qualquer conteúdo disponível esteja disponível indefinidamente.

## Mas como garantir a integridade do conteúdo?

Um dos grandes desafios do conteúdo digital é a garantia de autenticidade --
isto é, que o conteúdo vem da procedência correta -- e sua integridade -- isto
é que o conteúdo não foi adulterado.

Apesar deste ser um problema difícil e ainda não muito bem resolvido pelos arquivos digitais,
existem algumas maneiras de reduzi-lo.

Aqui damos dicas da mais fácil para a mais difícil:

1. Comparando múltiplas cópias salvas por pessoas diferentes baixadas de locais diferentes.
2. Assinando digitalmente o conteúdo baixado, o que requer mais conhecimento.

## Colaborando com este projeto

Este projeto é mantido [num repositório aberto](https://0xacab.org/arquivista/manifesto), onde você pode entrar em contato, sugerir e enviar mudanças, além de comunicar sobre suas próprias iniciativas.
